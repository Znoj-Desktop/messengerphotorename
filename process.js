"use strict";
const fs = require("fs");
const path = require("path");

const correctBeginningZero = timeString => ("0" + timeString).slice(-2);
const senderNameFolder = `DanielaKlicova_kOVH_l24jQ`;
const senderFirstName = `Daniela`; // could be undefined or null

const pathToPersonsRoot = `d:/Downloads/facebook-IriZnoj/messages/inbox/${senderNameFolder}`;
const newPath = `d:/Downloads/facebook-IriZnoj/${senderNameFolder}`;

//create dest folder if doesn't exists
if (!fs.existsSync(newPath)) {
  fs.mkdirSync(newPath);
} else {
  fs.readdir(newPath, (err, files) => {
    if (err) throw err;

    for (const file of files) {
      fs.unlink(path.join(newPath, file), err => {
        if (err) throw err;
      });
    }
  });
}

const files = fs
  .readdirSync(pathToPersonsRoot)
  .filter(file => file.match(/.*\.json/));
console.log(`Processing ${files.length} files...`);

for (const file of files) {
  // const file = files[0];
  const rawdata = fs.readFileSync(`${pathToPersonsRoot}/${file}`);
  const regex = new RegExp(senderFirstName ? `${senderFirstName} .*` : ".*");
  const messages = JSON.parse(rawdata).messages.filter(
    m => !!m.photos && m.sender_name.match(regex)
  );
  for (const message of messages) {
    // const message = messages[0];
    const photos = message.photos;
    for (const photo of photos) {
      // const photo = photos[0];
      const uriParts = (photo && photo.uri).match(
        /(.*)\/photos\/(.*\.(jpg|jpeg|gif|png))/
      );
      const uri = uriParts && uriParts.length >= 3 && uriParts[2];
      const dateTime = new Date(photo.creation_timestamp * 1000);
      const dateString = `${dateTime.getFullYear()}${correctBeginningZero(
        dateTime.getMonth() + 1
      )}${correctBeginningZero(dateTime.getDate())}`;
      const timeString = `${correctBeginningZero(
        dateTime.getHours()
      )}${correctBeginningZero(dateTime.getMinutes())}${correctBeginningZero(
        dateTime.getSeconds()
      )}`;
      const dateTimeString = `${dateString}_${timeString}`;
      // console.log(`uri: ${uri}`);
      // console.log(`dateTime: ${dateTimeString}`);
      let newFileName = `${newPath}/${dateTimeString}.${
        uriParts && uriParts.length >= 4 ? uriParts[3] : "jpg"
      }`;
      let index = 1;
      while (fs.existsSync(newFileName)) {
        newFileName = `${newPath}/${dateTimeString}_${index++}.${
          uriParts && uriParts.length >= 4 ? uriParts[3] : "jpg"
        }`;
      }
      index = 1;
      try {
        fs.copyFileSync(
          `${pathToPersonsRoot}/photos/${uri}`,
          newFileName,
          err => {
            if (err) {
              throw err;
            }
          }
        );
      } catch (e) {
        console.log(e.message);
      }
      // console.log(`${photo.uri} was copied to ${newFileName}`);
    }
  }

  console.log(
    `${
      files.length > 1
        ? Math.round((files.indexOf(file) * 100) / files.length)
        : 100
    }%`
  );
}
